use std::{
	collections::HashMap,
	sync::{Arc, RwLock},
};

use async_trait::async_trait;
use futures::stream::{BoxStream, StreamExt};
use tokio::{select, spawn};

type City = String;
type Temperature = u64;

#[async_trait]
pub trait Api: Send + Sync {
	async fn fetch(&self) -> Result<HashMap<City, Temperature>, String>;
	async fn subscribe(&self) -> BoxStream<Result<(City, Temperature), String>>;
}

pub struct StreamCache {
	results: Arc<RwLock<HashMap<String, u64>>>,
}

impl StreamCache {
	pub fn new(api: impl Api + 'static) -> Self {
		let instance = Self {
			results: Arc::new(RwLock::new(HashMap::new())),
		};
		instance.update_in_background(api);
		instance
	}

	pub fn get(&self, key: &str) -> Option<u64> {
		let Ok(results) = self.results.read() else {
			// Gotta do something about it :
			// - try to recover with into_inner
			// - make a new empty hashmap & fetch again
			eprintln!("poisoned lock");
			return None;
		};

		results.get(key).copied()
	}

	pub fn update_in_background(&self, api: impl Api + 'static) {
		let temps = self.results.clone();

		spawn(async move {
			let mut fetched = false;
			let mut subscription = api.subscribe().await;

			loop {
				select! {
					// Handle initial data (only once)
					fetch = api.fetch(), if !fetched => {
						match (fetch, temps.write()) {
							(Ok(data), Ok(mut temps)) => {
								// temps.extend(data) would erase latest data, use entries instead
								data.into_iter().for_each(|(city, temp)| {
									temps.entry(city).or_insert(temp);
								});
								fetched = true;
							},
							(_, Err(e)) => eprintln!("poisoned lock: {e}"),
							(Err(e), _) => eprintln!("fetch error: {e}"),
						}
					}
					// Handle subscription updates
					Some(update) = subscription.next() => {
						match (update, temps.write()) {
							// Insert new value or erase the old one
							(Ok((city, temp)), Ok(mut temps)) => drop(temps.insert(city, temp)),
							(_, Err(e)) => eprintln!("poisoned lock: {e}"),
							(Err(e), _) => eprintln!("subscription error: {e}"),
						}
					}
				}
			}
		});
	}
}

#[cfg(test)]
mod tests {
	use std::time::Duration;

	use futures::{future, stream::select, FutureExt, StreamExt};
	use maplit::hashmap;
	use tokio::{sync::Notify, time};

	use super::*;

	#[derive(Default)]
	struct TestApi {
		signal: Arc<Notify>,
	}

	#[async_trait]
	impl Api for TestApi {
		async fn fetch(&self) -> Result<HashMap<City, Temperature>, String> {
			// fetch is slow an may get delayed until after we receive the first updates
			self.signal.notified().await;
			Ok(hashmap! {
				"Berlin".to_string() => 29,
				"Paris".to_string() => 31,
			})
		}
		async fn subscribe(&self) -> BoxStream<Result<(City, Temperature), String>> {
			let results = vec![
				Ok(("London".to_string(), 27)),
				Ok(("Paris".to_string(), 32)),
			];
			select(
				futures::stream::iter(results),
				async {
					self.signal.notify_one();
					future::pending().await
				}
				.into_stream(),
			)
			.boxed()
		}
	}
	#[tokio::test]
	async fn works() {
		let cache = StreamCache::new(TestApi::default());

		// Allow cache to update
		time::sleep(Duration::from_millis(100)).await;

		assert_eq!(cache.get("Berlin"), Some(29));
		assert_eq!(cache.get("London"), Some(27));
		assert_eq!(cache.get("Paris"), Some(32));
	}
}
